#!/usr/bin/env python

import sys
import os
import requests
from BeautifulSoup import BeautifulSoup
from pymongo import MongoClient

htmlclient = {
"User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.56 Safari/536.5"
    }
filings = ["10-K", "10-Q"]
edgar = "http://www.sec.gov"
cik_list = [ cik.strip() for cik in open("CIKlist", "r") ]

# Assuming mongodb server is running at http://127.0.0.1:27017
client = MongoClient()
db = client.database
posts = db.posts

# Uncomment below code to unlease the daemon.
"""
def createDaemon(choice):
    if choice == 'start':
        try:
            pid = os.fork()
            if pid > 0:
               print 'PID: %s Daemon started successfully' % pid
               with open('pidinfo', 'w') as piddata:
                   piddata.write("%d" %pid)
               os._exit(0)
        except OSError, error:
            print 'Unable to fork. Error: %d (%s)' % (error.errno, error.strerror)
            os._exit(1)
        main()
    elif choice == 'stop':
        with open('pidinfo', 'r') as pid:
            os.kill(int(pid.read()), 15)
        print "Daemon killed succesfully"
    else:
        print "Wrong parameter."
        return
"""

# Uncomment below code to log all printed data to file.
"""
def log_it(logdata=None):
    with open("edgarpull.log", "a") as fp:
        fp.write(logdata)
    return
"""

def getdocs(cik=None, accno=None):
    """
    Getting the documents inside each filing type and each acc no if it is available
    """
    global edgar
    url = edgar + "/Archives/edgar/data/" + str(cik)  + "/" + accno + "-index.html"
    try:
        response = requests.get(url, headers=htmlclient)
        if response.status_code==200:
            soup = BeautifulSoup(response.text)
        else:
            return None, "No details"
    except requests.exceptions.ConnectionError:
        response = requests.get(url, verify=False, stream=True, headers=htmlclient)
        body = []
        for chunk in response.iter_content(1024):
            body.append(chunk)
        if response.status_code==200:
            soup = BeautifulSoup("".join(body))
        else:
            return None, "No details"
    finally:
        dates = [ s.text for s in soup.findAll("div", attrs={"class":"info"}) ]
        details = {}
        for table in soup.findAll("table", attrs={"summary":"Document Format Files"}):
            for trtag in table.findAll("tr"):
                tdtags = [ td for td in trtag.findAll("td") ]
                if tdtags:
                    detail = [ tdtags[0].text, tdtags[1].text, edgar+tdtags[2].a.get("href"), tdtags[3].text, tdtags[4].text ]
                    if tdtags[0].text == "&nbsp;":
                        details["blank"] = detail
                    else:
                        details[tdtags[0].text] = detail
 #                   detail["Document_link"] = edgar+tdtags[2].a.get("href")
 #                   detail["Type"] = tdtags[3].text
 #                   detail["Size"] = tdtags[4].text
 #                   if tdtags[0]:
 #                       detail["Sequence"] = tdtags[0].text
 #                       details[tdtags[0].text] = detail
 #                   else:
 #                       detail["Sequence"] = "blank"
 #                       details["blank"] = detail
        return dates, details

def getfilings(cik=None, filing=None):
    """
    get the search results for each cik and filing type
    """
    global edgar, htmlclient
    url = edgar + "/cgi-bin/browse-edgar?action=getcompany&dateb=&owner=exclude&count=100&CIK=" + str(cik) + "&type=" + str(filing)
    response = requests.get(url, headers=htmlclient)
    if response.status_code==200:
        soup = BeautifulSoup(response.text)
        # find accno, we can get description from it
        for acc in soup.findAll("td", attrs={"class":"small"}):
            dates, detail = getdocs(cik, acc.text.split(":")[1][1:21])
            if dates:
                date = dates[0]
            else:
                date = "No date"
            data = {"cik": cik,
                "description": acc.text,
                "accession no": acc.text.split(":")[1][1:21],
                "filing date": date,
                "filing type": filing,
                "filing detail": detail
                }
            if data:
                posts.insert(data)
                # comment below line to use daemonised code
                print("Inserted data for CIK:{0} filing:{1} accession no:{2}\n".format(cik, filing, acc.text.split(":")[1][1:21]))
                # Uncomment below line to use daemonised code
                #log_it("Inserted data for CIK:{0} filing:{1} accession no:{2}\n".format(cik, filing, acc.text.split(":")[1][1:21]))

def main():
    """
    The famous main function which mainly controls all the code here :p
    """
    try:
        global cik_list, filings
        for cik in cik_list:
            for filing in filings:
                getfilings(cik, filing)
    except KeyboardInterrupt:
        # comment below line to use daemonised code
        sys.exit(-1)
        # Uncomment below line use daemonised code
        #createDaemon("stop")

if __name__=='__main__':
    # comment below line to use daemonised code
    main()
    # Uncomment below line to use daemonised code
    """
    if len(sys.argv) == 2:
        createDaemon(sys.argv[1])
    else:
        print "Wrong parameter."
    """

